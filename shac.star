# Copyright 2023 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

# TODO(b/301136767): Don't assume these shared shac checks are always located here.
load("third_party/fuchsia-infra-bazel-rules/shac/checks/common.star", "register_checks")

register_checks()
