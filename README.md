Fuchsia GPU Driver Repository
=======================================

Getting Started
=======================================

To perform an initial bootstrap, execute the following:

1. In a terminal, clone the repository:

   ```posix-terminal
   git clone https://fuchsia.googlesource.com/drivers/graphics/gpu
   ```

1. Change to that directory:

   ```posix-terminal
   cd gpu
   ```

1. Fetch all git submodules:

   ```posix-terminal
   git submodule init && git submodule update --recursive
   ```

1. Bootstrap the build environment:

   ```posix-terminal
   scripts/bootstrap.sh
   ```

1. Fetch and build the SDK:

   ```posix-terminal
   tools/bazel build @fuchsia_sdk//:fuchsia_toolchain_sdk
   ```

See the [getting started](https://fuchsia.dev/fuchsia-src/get-started/sdk/get-started-with-driver)
guide for more info.

Building And Testing the ARM Mali GPU driver
=======================================

See [drivers/msd-arm-mali/README.md](drivers/msd-arm-mali/README.md).

