# Using the driver

## Loading the driver

```bash
tools/bazel run drivers/msd-arm-mali:pkg.component
 ```

## Running integration tests

After loading the driver, do

```bash
tools/bazel run drivers/msd-arm-mali:integration_tests
```

## Running conformance tests

After loading the driver, do

```bash
tools/bazel run drivers/msd-arm-mali:magma_conformance_tests
```
