// Copyright 2023 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include <fidl/fuchsia.driver.framework/cpp/fidl.h>
#include <fidl/fuchsia.hardware.gpu.mali/cpp/driver/wire.h>
#include <fidl/fuchsia.hardware.platform.device/cpp/wire.h>
#include <lib/driver/component/cpp/driver_base.h>
#include <lib/driver/component/cpp/driver_export.h>
#include <lib/driver/devfs/cpp/connector.h>
#include <lib/fit/thread_safety.h>
#include <lib/magma/platform/platform_bus_mapper.h>
#include <lib/magma/platform/zircon/zircon_platform_logger_dfv2.h>
#include <lib/magma/platform/zircon/zircon_platform_status.h>
#include <lib/magma/util/short_macros.h>
#include <lib/magma_service/sys_driver/magma_driver_base.h>

#include "parent_device_dfv2.h"

#if MAGMA_TEST_DRIVER
using MagmaDriverBaseType = msd::MagmaTestDriverBase;

zx_status_t magma_indriver_test(ParentDevice* device);

#else
using MagmaDriverBaseType = msd::MagmaProductionDriverBase;
#endif

class MaliDriver : public MagmaDriverBaseType {
 public:
  MaliDriver(fdf::DriverStartArgs start_args, fdf::UnownedSynchronizedDispatcher driver_dispatcher)
      : MagmaDriverBaseType("mali", std::move(start_args), std::move(driver_dispatcher)) {}

  zx::result<> MagmaStart() override {
    zx::result info_resource = GetInfoResource();
    // Info resource may not be available on user builds.
    if (info_resource.is_ok()) {
      magma::PlatformBusMapper::SetInfoResource(std::move(*info_resource));
    }

    parent_device_ = ParentDeviceDFv2::Create(incoming());
    if (!parent_device_) {
      MAGMA_LOG(ERROR, "Failed to create ParentDeviceDFv2");
      return zx::error(ZX_ERR_INTERNAL);
    }

    std::lock_guard lock(magma_mutex());

    set_magma_driver(msd::Driver::Create());
    if (!magma_driver()) {
      DMESSAGE("Failed to create MagmaDriver");
      return zx::error(ZX_ERR_INTERNAL);
    }

#if MAGMA_TEST_DRIVER
    set_unit_test_status(magma_indriver_test(parent_device_.get()));
#endif

    set_magma_system_device(msd::MagmaSystemDevice::Create(
        magma_driver(), magma_driver()->CreateDevice(parent_device_->ToDeviceHandle())));
    if (!magma_system_device()) {
      DMESSAGE("Failed to create MagmaSystemDevice");
      return zx::error(ZX_ERR_INTERNAL);
    }
    return zx::ok();
  }

  void Stop() override {
    MagmaDriverBaseType::Stop();
    magma::PlatformBusMapper::SetInfoResource(zx::resource{});
  }

 private:
  std::unique_ptr<ParentDeviceDFv2> parent_device_;
};

FUCHSIA_DRIVER_EXPORT(MaliDriver);
