# Vulkan Conformance Test Suite (CTS)

This subdirectory contains [Vulkan CTS](vulkan-cts) test targets that can be
used to validate drivers for vulkan conformance.
They are currently
[built and released as prebuilts in the fuchsia platform](platform-release) and
consumed downstream using the targets in this package.

## Testing Locally

Note: Only testing on sherlock and nelson is supported currently.

### Provision a test device

A sherlock/nelson with a minimal test product configuration should be used for
reliable results.

```bash
# Provision a sherlock test product.
bazel run //products:test_product.sherlock -- --no-bootloader-reboot

# OR: Provision a nelson test product.
bazel run //products:test_product.nelson -- --no-bootloader-reboot
```

The test product does not come preloaded with the OOT version of the MSD driver.
To load the OOT-built MSD driver, run the following:

```bash
bazel run drivers/msd-arm-mali:pkg.component
```

### Run tests

The different flavors of the Vulkan CTS (`arm64` vs `x64`; `no_args` vs
`unified` vs `zircon`) can be run locally with `bazel run`.

Example:

```bash
bazel run //common/tests/vulkan_cts:arm64_no_args
```

## Testing in Infrastructure

CLs can run the full Vulkan CTS in CQ by manually selecting the
`graphics-gpu-vulkan-cts` tryjob in the uploaded commit on gerrit.

[platform-release]: http://go/release-vulkan-cts-prebuilts
[vulkan-cts]: https://github.com/KhronosGroup/VK-GL-CTS/blob/main/external/vulkancts/README.md
