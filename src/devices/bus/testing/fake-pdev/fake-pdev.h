// Copyright 2021 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
#ifndef SRC_DEVICES_BUS_TESTING_FAKE_PDEV_FAKE_PDEV_H_
#define SRC_DEVICES_BUS_TESTING_FAKE_PDEV_FAKE_PDEV_H_

#include <fidl/fuchsia.hardware.platform.device/cpp/wire_test_base.h>
#include <lib/async/default.h>
#include <lib/mmio/mmio.h>

#include <atomic>
#include <map>
#include <optional>

struct pdev_mmio_t {
  // Offset from beginning of VMO where the mmio region begins.
  zx_off_t offset;
  // Size of mmio region.
  uint64_t size;
  zx_handle_t vmo;
};

struct pdev_device_info_t {
  uint32_t vid;
  uint32_t pid;
  uint32_t did;
  uint32_t mmio_count;
  uint32_t irq_count;
  uint32_t bti_count;
  uint32_t smc_count;
  uint32_t metadata_count;
  uint32_t reserved[8];
  char name[32];
};

struct pdev_board_info_t {
  // Vendor ID for the board.
  uint32_t vid;
  // Product ID for the board.
  uint32_t pid;
  // Board name from the boot image platform ID record,
  // (or from the BIOS on x86 platforms).
  char board_name[32];
  // Board specific revision number.
  uint32_t board_revision;
};

namespace fake_pdev {

struct MmioInfo {
  zx::vmo vmo;
  zx_off_t offset;
  size_t size;
};

using Mmio = std::variant<MmioInfo, fdf::MmioBuffer>;

class FakePDevFidl : public fidl::WireServer<fuchsia_hardware_platform_device::Device> {
 public:
  struct Config {
    // If true, a bti will be generated lazily if it does not exist.
    bool use_fake_bti = false;

    // If true, a smc will be generated lazily if it does not exist.
    bool use_fake_smc = false;

    // If true, an irq will be generated lazily if it does not exist.
    bool use_fake_irq = false;

    std::map<uint32_t, Mmio> mmios;
    std::map<uint32_t, zx::interrupt> irqs;
    std::map<uint32_t, zx::bti> btis;
    std::map<uint32_t, zx::resource> smcs;

    std::optional<pdev_device_info_t> device_info;
    std::optional<pdev_board_info_t> board_info;
  };

  FakePDevFidl() = default;

  fuchsia_hardware_platform_device::Service::InstanceHandler GetInstanceHandler(
      async_dispatcher_t* dispatcher = nullptr) {
    return fuchsia_hardware_platform_device::Service::InstanceHandler({
        .device = binding_group_.CreateHandler(
            this, dispatcher ? dispatcher : async_get_default_dispatcher(),
            fidl::kIgnoreBindingClosure),
    });
  }

  zx_status_t Connect(fidl::ServerEnd<fuchsia_hardware_platform_device::Device> request) {
    binding_group_.AddBinding(async_get_default_dispatcher(), std::move(request), this,
                              fidl::kIgnoreBindingClosure);
    return ZX_OK;
  }

  zx_status_t SetConfig(Config config) {
    config_ = std::move(config);
    return ZX_OK;
  }

 private:
  void GetMmioById(GetMmioByIdRequestView request, GetMmioByIdCompleter::Sync& completer) override;
  void GetMmioByName(GetMmioByNameRequestView request,
                     GetMmioByNameCompleter::Sync& completer) override;
  void GetInterruptById(GetInterruptByIdRequestView request,
                        GetInterruptByIdCompleter::Sync& completer) override;
  void GetInterruptByName(GetInterruptByNameRequestView request,
                          GetInterruptByNameCompleter::Sync& completer) override;
  void GetBtiById(GetBtiByIdRequestView request, GetBtiByIdCompleter::Sync& completer) override;
  void GetBtiByName(GetBtiByNameRequestView request,
                    GetBtiByNameCompleter::Sync& completer) override;
  void GetSmcById(GetSmcByIdRequestView request, GetSmcByIdCompleter::Sync& completer) override;
  void GetSmcByName(GetSmcByNameRequestView request,
                    GetSmcByNameCompleter::Sync& completer) override;
  void GetNodeDeviceInfo(GetNodeDeviceInfoCompleter::Sync& completer) override;
  void GetBoardInfo(GetBoardInfoCompleter::Sync& completer) override;
#if __Fuchsia_API_level__ > 30
  void GetPowerConfiguration(GetPowerConfigurationCompleter::Sync& completer) override;
#endif
  void handle_unknown_method(
      fidl::UnknownMethodMetadata<fuchsia_hardware_platform_device::Device> metadata,
      fidl::UnknownMethodCompleter::Sync& completer) override;

  Config config_;
  fidl::ServerBindingGroup<fuchsia_hardware_platform_device::Device> binding_group_;
};

}  // namespace fake_pdev

#endif  // SRC_DEVICES_BUS_TESTING_FAKE_PDEV_FAKE_PDEV_H_
