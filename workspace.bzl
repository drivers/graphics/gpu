# Copyright 2024 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

def gpu_deps(path = ""):
    """Set up dependencies of the GPU repository.

    Args:
        path: path where this repo is located, relative to the repo root."""

    if path != "":
        path_with_slash = path + "/"
    else:
        path_with_slash = path

    maybe(
        repo_rule = native.local_repository,
        name = "magma",
        path = path_with_slash + "common/magma",
    )

    maybe(
        repo_rule = native.new_local_repository,
        name = "Vulkan-Headers",
        build_file = "@gpu//:third_party/BUILD.Vulkan-Headers.bazel",
        path = path_with_slash + "third_party/Vulkan-Headers/",
    )

    maybe(
        repo_rule = native.local_repository,
        name = "HWCPipe",
        path = path_with_slash + "third_party/HWCPipe",
    )

    maybe(
        repo_rule = native.local_repository,
        name = "com_google_googletest",
        path = path_with_slash + "third_party/googletest",
    )
